# mealplanner

**mealplanner** is a web-based Flask application that is designed to be used, as the name implies, to help with meal planning. Users can add food components, save recipes, and build meal schedules. There is a level of customizable randomness built in to the application that keeps meal schedules interesting and always different.

## Installation

Simply use pip to install (in a virtual environment, of course!):

```bash
pip install mealplanner
```

## Usage

Windows (cmd.exe):
```bash
set FLASK_APP=mealplanner.py
set SECRET_KEY='a-secret-key-of-your-choosing'
python -m flask run
```

Unix (bash):
```bash
export FLASK_APP=mealplanner.py
export SECRET_KEY='a-secret-key-of-your-choosing'
python -m flask run
```

**NOTE**: To run the application in debug mode, use `set` (Windows) /`export` (Unix) `FLASK_ENVIRONMENT=development` prior to running the Flask app.

## Logical Objects

The following logical objects are used throughout the application:
* **Size**:      A relative indicator of how large a dish is.
* **Effort**:    A relative indicator of how hard it is to create the dish.
* **Cost**:      A relative indicator of the expense of a dish.
* **Label**:     Extra information used to highlight properties of a dish.
* **Category**:  A category used to differentiate dishes by purpose or type.
* **Component**: Individual food components that make up a meal or dish.
* **Dish**:      One or more components prepared to make a single dish.
* **Meal**:      A combination of components and dishes making up a meal.
* **Period**:    A sequence of dishes and meals to be eaten throughout a period (e.g. "Long Run Day" or "Work Day").
* **Schedule**:  A schedule of periods and extra components - the meal plan

## Routes

* `/index`: The homepage/dashboard
* `/dishes`: View all dishes
* `/dish/<label>`: View a random dish. Specify a label to constrain what kind of dish is returned.
* `/meals`: View all meals.
* `/meal/<count>`: View a random meal. Specify a number to retrieve more than one random meal.
