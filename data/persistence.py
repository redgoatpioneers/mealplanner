# -*- coding: utf-8 -*-
"""
This module is used as a generic interface to retrieve data from one or more
data repositories. Each function follows the format get_{object}, where
{object} can be one of the following types of data:
    sizes
    efforts
    costs
    labels
    categories
    components
    dishes
    meals
    periods
    schedules

Data is returned as JSON regardless of the type of repository being used.
You can also use get_database to get a full JSON dump of the entire database.

Currently this module is configured to pull from JSON files using json_repository.py.
"""

import os
from .json import json_repository as dm


dir_path = os.path.dirname(os.path.realpath(__file__))
json_path = os.path.join(dir_path, "json")


def get_sizes():
    """
    Retrieve size categories from the configured repository.

    Returns:
        list: a JSON array of size categories
    """
    return dm.get_sizes()


def get_categories():
    """
    Retrieve categories from the configured repository.

    Returns:
        list: a JSON array of categories
    """
    return dm.get_categories()


def get_costs():
    """
    Retrieve cost categories from the configured repository.

    Returns:
        list: a JSON array of cost categories
    """
    return dm.get_costs()


def get_efforts():
    """
    Retrieve effort categories from the configured repository.

    Returns:
        list: a JSON array of effort categories
    """
    return dm.get_efforts()


def get_labels():
    """
    Retrieve labels from the configured repository.

    Returns:
        list: a JSON array of labels
    """
    return dm.get_labels()


def get_templates():
    """
    Retrieve templates from the configured repository.

    Returns:
        list: a JSON array of templates
    """
    return dm.get_templates()


def get_static_properties():
    """
    Retrieve the values of the properties for dishes and meals that are restricted to specific choices.

    Returns:
        dict: A dictionary containing values for sizes, categories, costs, and efforts
    """
    return dm.get_static_properties()


def get_components(components: list = None):
    """
    Retrieve components from the configured repository.

    Args:
        components (list): (Optional) A list of specific components to retrieve.
    Returns:
        list: a JSON array of components and associated nutritional information
    """
    return dm.get_components(components)


def get_dish_names():
    """
    Retrieve dish names from the configured repository.

    Returns:
        list: a list of dish names
    """
    return dm.get_dish_names()


def get_dishes(dishes: list = None):
    """
    Retrieve dishes from the configured repository.

    Args:
        dishes (list): (Optional) A list of dish names (str) of specific dishes to retrieve.
    Returns:
        list: a JSON array of dishes
    """
    return dm.get_dishes(dishes)


def add_dish(dish: dict):
    """
    Create a dish and add it to the configured repository.

    Args:
        dish (dict): The JSON representation of a dish with the following properties:
            - name (string): A unique name for the dish.
            - size (string): The size of the dish (restricted selections).
            - category (string): A category for the dish (restricted selections).
            - cost (string): The relative cost of the dish (restricted selections).
            - effort (string): The relative effort for preparing the dish (restricted selections).
            - time (integer): The number of minutes it takes to prepare the dish.
            - labels (list): A list of labels (strings) for the dish (restricted selections).
            - serves (integer): The number of people the dish serves (4 means 4x 1 person or 2x 2 people).
            - components (list): A list of components (objects) that make up the dish (restricted selection).
            - seasoning (list): A list of seasonings (objects) that support the dish (restricted selection).
            - notes (string): Supporting notes or instructions for the dish, such as a recipe.
    Returns:
        str: Empty string if the dish was created successfully,
            otherwise a message indicating why the dish was not created.
    """
    all_dishes = get_dishes()

    # Make sure this is not a duplicate entry or name
    if dish['name'] in [d['name'] for d in all_dishes]:
        return 'Choose a unique name for the new dish.'

    # Make sure all required keys are provided and have at least some value or an empty list
    req_keys = ['name', 'size', 'category', 'cost', 'effort', 'time', 'labels', 'serves']
    for key in req_keys:
        if key not in dish.keys() or dish[key] is None or dish[key] == '':
            return 'Missing or invalid data for property {}'.format(key)

    # Make sure valid values are provided for size, category, cost, effort
    properties = get_static_properties()
    for key in ['size', 'category', 'cost', 'effort']:
        if dish[key] not in properties[key]:
            return 'Invalid value provided for property {}: {}'.format(key, dish[key])

    # Make sure the dish has at least one component
    if not dish['components'] or len(dish['components']) == 0:
        return 'This dish must have at least one component'

    return dm.add_dish(dish)


def get_meals(meals: list = None):
    """
    Retrieve meals from the configured repository.

    Args:
        meals (list): (Optional) A list of specific meals to retrieve.
    Returns:
        list: a JSON array of meals
    """
    return dm.get_meals(meals)


def add_meal(meal: dict):
    """
    Create a meal and add it to the configured repository.

    Args:
        meal (dict): The JSON representation of a meal with the following properties:
            - name (string): A unique name for the meal.
            - effort (string): The relative effort for preparing the meal (restricted selections).
            - categories (string): Zero or more categories for the meal, each being replaced with a random dish
                of that category (restricted selections).
            - cost (string): The relative cost of the meal (restricted selections).
            - time (integer): The number of minutes it takes to prepare the meal.
            - labels (list): A list of labels (strings) for the meal (restricted selections).
            - serves (integer): The number of people the meal serves (4 means 4x 1 person or 2x 2 people).
            - components (list): A list of components (objects) that make up the meal (restricted selection).
            - seasoning (list): A list of seasonings (objects) that support the meal (restricted selection).
            - notes (string): Supporting notes or instructions for the meal, such as a recipe.
    Returns:
        str: Empty string if the meal was created successfully,
            otherwise a message indicating why the meal was not created.
    """
    all_meals = get_meals()

    # Make sure this is not a duplicate entry or name
    if meal['name'] in [d['name'] for d in all_meals]:
        return 'Choose a unique name for the new meal.'

    # Make sure all required keys are provided and have at least some value or an empty list
    req_keys = ['name', 'effort']
    for key in req_keys:
        if key not in meal.keys() or meal[key] is None or meal[key] == '':
            return 'Missing or invalid data for property {}'.format(key)

    # Make sure valid values are provided for effort
    efforts = get_efforts()
    if meal['effort'] not in efforts:
        return 'Invalid value provided for effort: {}'.format(meal['effort'])

    # Make sure the meal has at least one component or dish
    if (not meal['components'] or len(meal['components']) == 0) and (not meal['dishes'] or len(meal['dishes']) == 0):
        return 'This meal must have at least one component or dish.'

    return dm.add_meal(meal)


def get_periods(periods: list = None):
    """
    Retrieve periods from the configured repository.

    Args:
        periods (list): (Optional) A list of specific periods to retrieve.
    Returns:
        list: a JSON array of periods
    """
    return dm.get_periods(periods)


def add_period(period: dict):
    """
        Create a period and add it to the configured repository.

        Args:
            period (dict): The JSON representation of a period with the following properties:
                - name (string): A unique name for the period.
                - template (string): The template used to create this period (if relevant).
                - sequence (dict): An order-indexed dictionary of food items.
        Returns:
            str: Empty string if the period was created successfully,
                otherwise a message indicating why the period was not created.
    """
    all_periods = get_periods()

    # Make sure this is not a duplicate entry or name
    if period['name'] in [d['name'] for d in all_periods]:
        return 'Choose a unique name for the new period.'

    # Make sure all required keys are provided and have at least some value or an empty list
    req_keys = ['name', 'template']
    for key in req_keys:
        if key not in period.keys() or period[key] is None or period[key] == '':
            return 'Missing or invalid data for property {}'.format(key)

    # Make sure valid values are provided for size, category, cost, effort
    properties = get_static_properties()
    for i, sequence in period['sequence'].items():
        for key in ['size', 'category', 'cost', 'effort']:
            if sequence[key] not in properties[key] and sequence[key] is not None:
                return 'Invalid value provided for sequence {}, property {}: {}'\
                    .format(int(i)+1, key, sequence[key])
    
    return dm.add_period(period)


def get_schedules(schedules: list = None):
    """
    Retrieve schedules from the configured repository.

    Args:
        schedules (list): (Optional) A list of specific schedules to retrieve.
    Returns:
        list: a JSON array of schedules
    """
    return dm.get_schedules(schedules)


def add_schedule(schedule: dict):
    """
        Create a schedule and add it to the configured repository.

        Args:
            schedule (dict): The JSON representation of a schedule with the following properties:
                - name (string): A unique name for the schedule.
                - template (string): The template used to create this schedule (if relevant).
                - schedule (dict): A name-indexed dictionary of period items.
        Returns:
            str: Empty string if the schedule was created successfully,
                otherwise a message indicating why the schedule was not created.
    """
    all_schedules = get_schedules()

    # Make sure this is not a duplicate entry or name
    if schedule['name'] in [d['name'] for d in all_schedules]:
        return 'Choose a unique name for the new schedule.'

    # Make sure all required keys are provided and have at least some value or an empty list
    req_keys = ['name', 'template']
    for key in req_keys:
        if key not in schedule.keys() or schedule[key] is None or schedule[key] == '':
            return 'Missing or invalid data for property {}'.format(key)
    
    return dm.add_schedule(schedule)


def get_database():
    """
    Return all of the data in the configured repository (i.e. full export).

    Returns:
        dict: A JSON object containing all of the data in the JSON database.
    """
    return dm.get_database()
