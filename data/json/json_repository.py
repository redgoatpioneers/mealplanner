# -*- coding: utf-8 -*-
"""
This module is used to retrieve JSON data from the built-in JSON files saved
as part of the repository. Each function follows the format get_{object}, where
{object} can be one of the following types of data:
    sizes
    efforts
    costs
    labels
    categories
    components
    dishes
    meals
    periods
    schedules

You can also use get_database to get a full JSON dump of the entire database.
"""

import os
import json


dir_path = os.path.dirname(os.path.realpath(__file__))


def get_sizes():
    """
    Retrieve size categories from the built-in JSON data files.

    Returns:
        list: a JSON array of size categories
    """
    with open(os.path.join(dir_path, "sizes.json"), 'r+') as file:
        all_sizes = json.load(file)
    return all_sizes


def get_categories():
    """
    Retrieve categories from the built-in JSON data files.

    Returns:
        list: a JSON array of categories
    """
    with open(os.path.join(dir_path, "categories.json"), 'r+') as file:
        all_categories = json.load(file)
    return all_categories


def get_costs():
    """
    Retrieve cost categories from the built-in JSON data files.

    Returns:
        list: a JSON array of cost categories
    """
    with open(os.path.join(dir_path, "costs.json"), 'r+') as file:
        all_costs = json.load(file)
    return all_costs


def get_efforts():
    """
    Retrieve effort categories from the built-in JSON data files.

    Returns:
        list: a JSON array of effort categories
    """
    with open(os.path.join(dir_path, "efforts.json"), 'r+') as file:
        all_efforts = json.load(file)
    return all_efforts


def get_labels():
    """
    Retrieve labels from the built-in JSON data files.

    Returns:
        list: a JSON array of labels
    """
    with open(os.path.join(dir_path, "labels.json"), 'r+') as file:
        all_labels = json.load(file)
    return all_labels


def get_templates():
    """
    Retrieve templates from the built-in JSON data files.

    Returns:
        list: a JSON array of labels
    """
    with open(os.path.join(dir_path, "templates.json"), 'r+') as file:
        all_templates = json.load(file)
    return all_templates


def get_static_properties():
    """
    Retrieve the values of the properties for dishes and meals that are restricted to specific choices.

    Returns:
        dict: A dictionary containing values for sizes, categories, costs, and efforts
    """
    properties = {
        'size': get_sizes(),
        'category': get_categories(),
        'cost': get_costs(),
        'effort': get_efforts()
    }
    return properties


def get_components(components: list = None):
    """
    Retrieve components from the built-in JSON data files.

    Args:
        components (list): (Optional) A list of specific components to retrieve.
    Returns:
        list: a JSON array of components and associated nutritional information
    """
    with open(os.path.join(dir_path, "components.json"), 'r+') as file:
        all_components = json.load(file)
    if components is not None:
        return [x for x in all_components if x in components]
    else:
        return all_components


def get_dish_names():
    """
    Retrieve dish names from the built-in JSON data files.

    Returns:
        list: a list of dish names
    """
    with open(os.path.join(dir_path, "dishes.json"), 'r+') as file:
        all_dishes = json.load(file)
    return [d['name'] for d in all_dishes]


def get_dishes(dishes: list = None):
    """
    Retrieve dishes from the built-in JSON data files.

    Args:
        dishes (list): (Optional) A list of dish names (str) of specific dishes to retrieve.
    Returns:
        list: a JSON array of dishes
    """
    with open(os.path.join(dir_path, "dishes.json"), 'r+') as file:
        all_dishes = json.load(file)
    if dishes is not None:
        return [d for d in all_dishes if d["name"] in dishes]
    else:
        return all_dishes


def add_dish(dish: dict):
    """
    Create a dish and add it to the built-in JSON data file.

    Args:
        dish (dict): The JSON representation of a dish with the following properties:
            - name (string): A unique name for the dish.
            - size (string): The size of the dish (restricted selections).
            - category (string): A category for the dish (restricted selections).
            - cost (string): The relative cost of the dish (restricted selections).
            - effort (string): The relative effort for preparing the dish (restricted selections).
            - time (integer): The number of minutes it takes to prepare the dish.
            - labels (list): A list of labels (strings) for the dish (restricted selections).
            - serves (integer): The number of people the dish serves (4 means 4x 1 person or 2x 2 people).
            - components (list): A list of components (objects) that make up the dish (restricted selection).
            - seasoning (list): A list of seasonings (objects) that support the dish (restricted selection).
            - notes (string): Supporting notes or instructions for the dish, such as a recipe.
    Returns:
        str: Empty string if the dish was created successfully,
            otherwise a message indicating why the dish was not created.
    """
    with open(os.path.join(dir_path, 'dishes.json'), 'r+') as file:
        all_dishes = json.load(file)

    all_dishes.append(dish)
    all_dishes.sort(key=lambda d: d['name'])
    with open(os.path.join(dir_path, 'dishes.json'), 'w+') as file:
        file.write(json.dumps(all_dishes, indent=4))
    return ""


def get_meals(meals: list = None):
    """
    Retrieve meals from the built-in JSON data files.

    Args:
        meals (list): (Optional) A list of specific meals to retrieve.
    Returns:
        list: a JSON array of meals
    """
    with open(os.path.join(dir_path, "meals.json"), 'r+') as file:
        all_meals = json.load(file)
    if meals is not None:
        return [x for x in all_meals if x["name"] in meals]
    else:
        return all_meals


def add_meal(meal: dict):
    """
    Create a meal and add it to the built-in JSON data file.

    Args:
        meal (dict): The JSON representation of a meal with the following properties:
            - name (string): A unique name for the meal.
            - effort (string): The relative effort for preparing the meal (restricted selections).
            - categories (string): Zero or more categories for the meal, each being replaced with a random dish
                of that category (restricted selections).
            - cost (string): The relative cost of the meal (restricted selections).
            - time (integer): The number of minutes it takes to prepare the meal.
            - labels (list): A list of labels (strings) for the meal (restricted selections).
            - serves (integer): The number of people the meal serves (4 means 4x 1 person or 2x 2 people).
            - components (list): A list of components (objects) that make up the meal (restricted selection).
            - seasoning (list): A list of seasonings (objects) that support the meal (restricted selection).
            - notes (string): Supporting notes or instructions for the meal, such as a recipe.
    Returns:
        str: Empty string if the meal was created successfully,
            otherwise a message indicating why the meal was not created.
    """
    with open(os.path.join(dir_path, 'meals.json'), 'r+') as file:
        all_meals = json.load(file)

    all_meals.append(meal)
    all_meals.sort(key=lambda d: d['name'])
    with open(os.path.join(dir_path, 'meals.json'), 'w+') as file:
        file.write(json.dumps(all_meals, indent=4))
    return ""


def get_periods(periods: list = None):
    """
    Retrieve periods from the built-in JSON data files.

    Args:
        periods (list): (Optional) A list of specific periods to retrieve.
    Returns:
        list: a JSON array of periods
    """
    with open(os.path.join(dir_path, "periods.json"), 'r+') as file:
        all_periods = json.load(file)
    if periods is not None:
        return [x for x in all_periods if x["name"] in periods]
    else:
        return all_periods


def add_period(period: dict):
    """
    Create a period and add it to the built-in JSON data file.

    Args:
            period (dict): The JSON representation of a period with the following properties:
                - name (string): A unique name for the period.
                - template (string): The template used to create this period (if relevant).
                - sequence (dict): An order-indexed dictionary of food items.
    Returns:
        str: Empty string if the period was created successfully,
            otherwise a message indicating why the period was not created.
    """
    with open(os.path.join(dir_path, 'periods.json'), 'r+') as file:
        all_periods = json.load(file)

    all_periods.append(period)
    all_periods.sort(key=lambda d: d['name'])
    with open(os.path.join(dir_path, 'periods.json'), 'w+') as file:
        file.write(json.dumps(all_periods, indent=4))
    return ""


def get_schedules(schedules: list = None):
    """
    Retrieve schedules from the built-in JSON data files.

    Args:
        schedules (list): (Optional) A list of specific schedules to retrieve.
    Returns:
        list: a JSON array of schedules
    """
    with open(os.path.join(dir_path, "schedules.json"), 'r+') as file:
        all_schedules = json.load(file)
    if schedules is not None:
        return [x for x in all_schedules if x["name"] in schedules]
    else:
        return all_schedules


def add_schedule(schedule: dict):
    """
    Create a schedule and add it to the built-in JSON data file.

    Args:
        schedule (dict): The JSON representation of a schedule with the following properties:
            - name (string): A unique name for the schedule.
            - template (string): The template used to create this schedule (if relevant).
            - schedule (dict): A name-indexed dictionary of period items.
    Returns:
        str: Empty string if the schedule was created successfully,
            otherwise a message indicating why the schedule was not created.
    """
    with open(os.path.join(dir_path, "schedules.json"), 'r+') as file:
        all_schedules = json.load(file)
    all_schedules.append(schedule)
    all_schedules.sort(key=lambda d: d['name'])
    with open(os.path.join(dir_path, 'schedules.json'), 'w+') as file:
        file.write(json.dumps(all_schedules, indent=4))
    return ""


def get_database():
    """
    Return all of the data in the JSON database (i.e. full export).

    Returns:
        dict: A JSON object containing all of the data in the JSON database.
    """
    return {
        "components": get_components(),
        "sizes": get_sizes(),
        "efforts": get_efforts(),
        "costs": get_costs(),
        "labels": get_labels(),
        "categories": get_categories(),
        "dishes": get_dishes(),
        "meals": get_meals(),
        "periods": get_periods(),
        "schedules": get_schedules()
    }
