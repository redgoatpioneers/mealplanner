PYTHON = python

.DEFAULT: help
help:
    @echo "make test"
    @echo "    Run pytest unit tests with flake8"

test:
    pytest --flake8 --cov=mealplanner --cov-report term-missing