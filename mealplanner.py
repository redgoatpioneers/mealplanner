# -*- coding: utf-8 -*-
"""
mealplanner is a web-based Flask application used, as the name implies, to help
with meal planning. Users can add food components, save recipes, and build
meal schedules. There is a level of customizable randomness built in to the
application that keeps meal schedules interesting and always different.

The following logical objects are used throughout the application:
    sizes:      A relative indicator of how large a dish is.
    efforts:    A relative indicator of how hard it is to create the dish.
    costs:      A relative indicator of the expense of a dish.
    labels:     Extra information used to highlight properties of a dish.
    categories: A category used to differentiate dishes by purpose or type.
    components: Individual food components that make up a meal or dish.
    dishes:     One or more components prepared to make a single dish.
    meals:      A combination of components and dishes making up a meal.
    periods:    A sequence of dishes and meals to be eaten throughout a period.
    schedules:  A schedule of periods and extra components - the meal plan.
"""

from . import __version__
from flask import Flask, render_template, flash, redirect
from config import Config
from data import persistence as db
from logic.lottery import get_random_dish, get_random_meals
from logic.planner import build_mealplan_from_schedule as bp
from forms.forms import DishForm, MealForm, PeriodForm, ScheduleForm

app = Flask(__name__)
app.config.from_object(Config)


def get_full_meal(meal):
    if "dishes" in meal.keys():
        meal_dishes = []
        for dish in meal["dishes"]:
            full_dish = db.get_dishes(dish)[0]
            meal_dishes.append(full_dish)
        meal["dishes"] = meal_dishes
    return meal


@app.context_processor
def product_version():
    return dict(version=__version__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/dishes')
def dishes():
    all_dishes = db.get_dishes()
    return render_template('dishes.html', dishes=all_dishes)


@app.route('/dish')
@app.route('/dish/<label>')
def random_dish(label=None):
    dish = get_random_dish() if label is None else get_random_dish([label])
    return render_template('dishes.html', dishes=[dish])


@app.route('/dishes/new')
def create_dishes():
    return redirect('/dish/new')


@app.route('/dish/new', methods=['GET', 'POST'])
def create_dish():
    form = DishForm()
    if form.validate_on_submit():
        response = db.add_dish(form.get_dish())
        if response == "":
            flash('Created new dish "{}"'.format(form.name.data))
            return redirect('/dishes')
        else:
            flash(response)
    else:
        if form.errors:
            flash(str.join("; ", ["Error with field {0}: {1}".format(k, v) for k, v in form.errors.items()]))
    return render_template('new_dish.html', form=form)


@app.route('/dish/modify')
@app.route('/dish/modify/<dish>')
def modify_dish(dish: str = None):
    dish = db.get_dishes([dish])[0] if dish is not None else None
    return render_template('new_dish.html', dish=dish)


@app.route('/meals')
def meals():
    all_meals = db.get_meals()
    full_meals = []
    for meal in all_meals:
        full_meals.append(get_full_meal(meal))
    return render_template('meals.html', meals=full_meals)


@app.route('/meal')
@app.route('/meals/<count>')
def random_meal(count=1):
    random_meals = get_random_meals(int(count))
    full_meals = []
    for meal in random_meals:
        full_meals.append(get_full_meal(meal))
    return render_template('meals.html', meals=full_meals)


@app.route('/meals/new')
def create_meals():
    return redirect('/meal/new')


@app.route('/meal/new', methods=['GET', 'POST'])
def create_meal():
    form = MealForm()
    if form.validate_on_submit():
        response = db.add_meal(form.get_meal())
        if response == "":
            flash('Created new meal "{}"'.format(form.name.data))
            return redirect('/meals')
        else:
            flash(response)
    else:
        if form.errors:
            flash(str.join("; ", ["Error with field {0}: {1}".format(k, v) for k, v in form.errors.items()]))
    return render_template('new_meal.html', form=form)


@app.route('/periods')
def periods():
    all_periods = db.get_periods()
    return render_template('periods.html', periods=all_periods)


@app.route('/periods/new', methods=['GET', 'POST'])
def create_period():
    form = PeriodForm()
    if form.validate_on_submit():
        response = db.add_period(form.get_period())
        if response == "":
            flash('Created new period "{}"'.format(form.name.data))
            return redirect('/periods')
        else:
            flash(response)
    else:
        if form.errors:
            flash(str.join("; ", ["Error with field {0}: {1}".format(k, v) for k, v in form.errors.items()]))
    return render_template('new_period.html', form=form)


@app.route('/schedules')
def schedules():
    all_schedules = db.get_schedules()
    return render_template('schedules.html', schedules=all_schedules)


@app.route('/schedules/new', methods=['GET', 'POST'])
def create_schedule():
    form = ScheduleForm()
    if form.validate_on_submit():
        response = db.add_schedule(form.get_schedule())
        if response == "":
            flash('Created new schedule "{}"'.format(form.name.data))
            return redirect('/schedules')
        else:
            flash(response)
    else:
        if form.errors:
            flash(str.join("; ", ["Error with field {0}: {1}".format(k, v) for k, v in form.errors.items()]))
    return render_template('new_schedule.html', form=form)


@app.route('/mealplan/demo')
def demo_mealplan():
    schedule = bp('Demo Schedule', 'Demo Meal Plan: Standard Week')
    return render_template('mealplan.html', schedule=schedule)


if __name__ == '__main__':
    app.run()
