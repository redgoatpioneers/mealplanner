from flask_wtf import FlaskForm
from wtforms import (
    Form, FormField, FieldList,
    StringField, TextAreaField, SelectField, SelectMultipleField, IntegerField, DecimalField, BooleanField,
    SubmitField)
from wtforms.validators import DataRequired, Length
from data import persistence as db
import decimal


SELECT_TEXT = 'Select'


def get_dummy_component(name: str):
    return {
        'component': name,
        'quantity': 1,
        'unit': 'each',
        'state': 'default'
    }


class ComponentSubform(Form):
    """
    This subform is used to generate components and seasonings for dishes and meals.

    CSRF is disabled for this subform (using `Form` as parent class) because
    it is never used by itself.
    """
    components = [(c.lower(), c) for c in db.get_components()]
    components.append(('selectComponent', SELECT_TEXT))
    component = SelectField('Component', choices=components, default='selectComponent')
    quantity = DecimalField('Quantity', places=3, rounding=decimal.ROUND_05UP, default=1)
    unit = StringField('Unit', default='each', validators=[
        DataRequired(), Length(min=1, max=40, message='Name must contain 1-40 characters.')])
    state = StringField('State', default='raw', validators=[
        Length(min=0, max=40, message='Name must contain 1-40 characters.')])
    variety = StringField('Variety', validators=[
        Length(min=0, max=40, message='Name must contain 1-40 characters.')])


class DishForm(FlaskForm):
    name = StringField('Name', validators=[
        DataRequired(), Length(min=1, max=255, message='Name must contain 1-255 characters.')])
    size = SelectField('Size', choices=[(s.lower(), s) for s in db.get_sizes()])
    category = SelectField('Category', choices=[(c.lower(), c) for c in db.get_categories()])
    cost = SelectField('Cost', choices=[(c.lower(), c) for c in db.get_costs()])
    effort = SelectField('Effort', choices=[(e.lower(), e) for e in db.get_efforts()])
    time = IntegerField('Time')
    labels = SelectMultipleField('Labels', choices=[(l.lower(), l) for l in db.get_labels()])
    serves = IntegerField('Serves')
    components = FieldList(FormField(ComponentSubform), min_entries=1)
    seasoning = FieldList(FormField(ComponentSubform), min_entries=1)
    notes = TextAreaField('Notes', validators=[
        Length(min=0, max=4096, message='Name must contain no more than 4096 characters.')])
    no_seasoning = BooleanField('None', default=True)

    def get_dish(self):
        """
        Generate a dish object from the properties of the form.

        Returns:
            (dict): A dish object.
        """
        component_list = [
            {
                'component': dict(c.component.choices).get(c.component.data),
                'quantity': str(c.quantity.data),
                'unit': c.unit.data,
                'state': c.state.data,
                'variety': c.variety.data,
            } for c in self['components']
        ]
        seasoning_list = [
            {
                'component': dict(c.component.choices).get(c.component.data),
                'quantity': str(c.quantity.data),
                'unit': c.unit.data,
                'state': c.state.data,
                'variety': c.variety.data,
            } for c in self['seasoning']
        ] if not self.no_seasoning else []
        return {
            'name': self.name.data,
            'size': dict(self.size.choices).get(self.size.data),
            'category': dict(self.category.choices).get(self.category.data),
            'cost': dict(self.cost.choices).get(self.cost.data),
            'effort': dict(self.effort.choices).get(self.effort.data),
            'time': self.time.data,
            'labels': [dict(self.labels.choices).get(choice) for choice in self.labels.data],
            'serves': self.serves.data,
            'components': [c for c in filter(lambda c: c['component'] != SELECT_TEXT, component_list)],
            'seasoning': [c for c in filter(lambda c: c['component'] != SELECT_TEXT, seasoning_list)] or [],
            'notes': self.notes.data
        }

    submit = SubmitField('Create Dish')


class MealForm(FlaskForm):
    name = StringField('Name', validators=[
        DataRequired(), Length(min=1, max=255, message='Name must contain 1-255 characters.')])
    effort = SelectField('Effort', choices=[(e.lower(), e) for e in db.get_efforts()])
    dishes = SelectMultipleField('Dishes', choices=[(d.lower(), d) for d in db.get_dish_names()])
    categories = SelectMultipleField('Categories', choices=[(c.lower(), c) for c in db.get_categories()])
    components = FieldList(FormField(ComponentSubform), min_entries=1)
    notes = TextAreaField('Notes', validators=[
        Length(min=0, max=4096, message='Name must contain no more than 4096 characters.')])
    no_components = BooleanField('None', default=True)

    def get_meal(self):
        """
        Generate a meal object from the properties of the form.

        Returns:
            (dict): A meal object.
        """
        component_list = [
            {
                'component': dict(c.component.choices).get(c.component.data),
                'quantity': str(c.quantity.data),
                'unit': c.unit.data,
                'state': c.state.data,
                'variety': c.variety.data,
            } for c in self['components']
        ] if not self.no_components.data else []
        return {
            'name': self.name.data,
            'effort': dict(self.effort.choices).get(self.effort.data),
            'categories': [dict(self.categories.choices).get(choice) for choice in self.categories.data],
            'dishes': [dict(self.dishes.choices).get(choice) for choice in self.dishes.data],
            'components': [c for c in filter(lambda c: c['component'] != SELECT_TEXT, component_list)],
            'notes': self.notes.data if self.notes.data else ''
        }

    submit = SubmitField('Create Meal')


class SequenceSubform(Form):
    """
    This subform is used to generate sequences for periods.

    CSRF is disabled for this subform (using `Form` as parent class) because
    it is never used by itself.
    """
    types = ['Component', 'Dish', 'Meal']

    sizes = [(s.lower(), s) for s in db.get_sizes()]
    sizes.append(('selectComponent', SELECT_TEXT))

    efforts = [(e.lower(), e) for e in db.get_efforts()]
    efforts.append(('selectComponent', SELECT_TEXT))

    costs = [(c.lower(), c) for c in db.get_costs()]
    costs.append(('selectComponent', SELECT_TEXT))

    categories = [(c.lower(), c) for c in db.get_categories()]
    categories.append(('selectComponent', SELECT_TEXT))

    labels = [(l.lower(), l) for l in db.get_labels()]

    sequence_name = StringField('Name', validators=[
        DataRequired(), Length(min=1, max=255, message='Name must contain 1-255 characters.')])
    sequence_type = SelectField('Type', choices=[(t.lower(), t) for t in types], default='Meal')
    size = SelectField('Size', choices=sizes, default='selectComponent')
    effort = SelectField('Effort', choices=efforts, default='selectComponent')
    cost = SelectField('Cost', choices=costs, default='selectComponent')
    category = SelectField('Category', choices=categories, default='selectComponent')
    sequence_labels = SelectMultipleField('Labels', choices=labels)


class PeriodForm(FlaskForm):
    name = StringField('Name', validators=[
        DataRequired(), Length(min=1, max=255, message='Name must contain 1-255 characters.')])
    template = SelectField('Template', choices=[(t.lower(), t) for t in db.get_templates()])
    sequence = FieldList(FormField(SequenceSubform), min_entries=1)

    def get_period(self):
        """
        Generate a period object from the properties of the form.

        Returns:
            (dict): A period object.
        """
        sequence_list = {
            i: {
                'name': s.sequence_name.data,
                'type': dict(s.sequence_type.choices).get(s.sequence_type.data),
                'size': None if s.size.data == 'selectComponent'
                else dict(s.size.choices).get(s.size.data),
                'effort': None if s.effort.data == 'selectComponent'
                else dict(s.effort.choices).get(s.effort.data),
                'cost': None if s.cost.data == 'selectComponent'
                else dict(s.cost.choices).get(s.cost.data),
                'category': None if s.category.data == 'selectComponent'
                else dict(s.category.choices).get(s.category.data),
                'labels': None if s.sequence_labels.data == 'selectComponent'
                else [dict(s.sequence_labels.choices).get(choice) for choice in s.sequence_labels.data]
            } for i, s in enumerate(self['sequence'])
        }
        return {
            'name': self.name.data,
            'template': dict(self.template.choices).get(self.template.data),
            'sequence': sequence_list
        }

    submit = SubmitField('Create Period')


class PeriodSubform(Form):
    """
    This subform is used to generate periods for schedules.

    CSRF is disabled for this subform (using `Form` as parent class) because
    it is never used by itself.
    """
    period_name = StringField('Name', validators=[
        DataRequired(), Length(min=1, max=255, message='Name must contain 1-255 characters.')])
    period = SelectField('Period', choices=[(p['name'].lower(), p['name']) for p in db.get_periods()])


class ScheduleForm(FlaskForm):
    name = StringField('Name', validators=[
        DataRequired(), Length(min=1, max=255, message='Name must contain 1-255 characters.')])
    template = SelectField('Template', choices=[(t.lower(), t) for t in db.get_templates()])
    schedule = FieldList(FormField(PeriodSubform), min_entries=1)

    def get_schedule(self):
        """
        Generate a schedule object from the properties of the form.

        Returns:
            (dict): A schedule object.
        """
        schedule_list = {s.period_name.data: dict(s.period.choices).get(s.period.data) for s in self['schedule']}
        return {
            'name': self.name.data,
            'template': dict(self.template.choices).get(self.template.data),
            'schedule': schedule_list
        }

    submit = SubmitField('Create Schedule')
