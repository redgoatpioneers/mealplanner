from distutils.core import setup
from __init__ import __version__


setup(
    name='mealplanner',
    version=__version__,
    license='MIT License',
    long_description=open('README.md').read(),
    install_requires=[
        "Flask==1.1.1"
    ]
)
