from data.json.json_repository import (
    get_components, get_sizes, get_efforts, get_costs, get_labels,
    get_categories, get_dishes, get_meals, get_database)


def test_get_components_returns_known_components():
    known_components = ["Bread", "Butter"]
    all_components = get_components()
    for component in known_components:
        assert component in all_components


def test_get_components_filtered():
    known_components = ["Bread"]
    components_filter = ["Butter"]
    all_components = get_components(components_filter)
    for component in known_components:
        assert component not in all_components


def test_get_sizes_returns_known_sizes():
    known_sizes = ["Snack", "Side", "Meal"]
    all_sizes = get_sizes()
    for size in known_sizes:
        assert size in all_sizes


def test_get_efforts_returns_known_efforts():
    known_efforts = ["Easy", "Medium", "Difficult"]
    all_efforts = get_efforts()
    for effort in known_efforts:
        assert effort in all_efforts


def test_get_costs_returns_known_costs():
    known_costs = ["Cheap", "Normal", "Expensive"]
    all_costs = get_costs()
    for cost in known_costs:
        assert cost in all_costs


def test_get_labels_returns_known_labels():
    known_labels = ["Bulk", "Breakfast"]
    all_labels = get_labels()
    for label in known_labels:
        assert label in all_labels


def test_get_categories_returns_known_categories():
    known_categories = ["Pasta", "Sauce"]
    all_categories = get_categories()
    for category in known_categories:
        assert category in all_categories


def test_get_dishes_returns_known_dishes():
    known_dishes = ["Potato Curry", "Pad Thai", "Black Bean Burgers"]
    all_dishes = get_dishes()
    for dish in known_dishes:
        assert dish in [d["name"] for d in all_dishes]


def test_get_dishes_filtered():
    known_dishes = ["Yogurt and Granola", "Quick Oatmeal"]
    dish_filter = ["Potato Curry", "Pad Thai", "Black Bean Burgers"]
    all_dishes = get_dishes(dish_filter)
    for dish in known_dishes:
        assert dish not in [d["name"] for d in all_dishes]


def test_get_dishes_has_all_keys():
    keys = [
        "name", "size", "category", "cost", "effort",
        "time", "labels", "serves", "components"]
    all_dishes = get_dishes()
    for dish in all_dishes:
        for key in keys:
            assert key in dish.keys()


def test_get_meals_returns_known_meals():
    known_meals = ["Potato Curry and Rice", "Vegetable Soup", "Black Bean Burgers"]
    all_meals = get_meals()
    for meal in known_meals:
        assert meal in [m["name"] for m in all_meals]


def test_get_meals_filtered():
    known_meals = ["Potato Curry and Rice", "Vegetable Soup", "Black Bean Burgers"]
    meal_filter = ["Sandwich and Salad", "Chili and Cornbread"]
    all_meals = get_meals(meal_filter)
    for meal in known_meals:
        assert meal not in [m["name"] for m in all_meals]


def test_get_meals_has_all_keys():
    all_meals = get_meals()
    for meal in all_meals:
        assert "name" in meal.keys()
        assert ("dishes" in meal.keys() or
                "components" in meal.keys() or
                "categories" in meal.keys())


def test_get_databases():
    assert get_database()


def test_get_database_has_all_keys():
    keys = [
        "sizes", "efforts", "costs", "labels", "categories",
        "components", "dishes", "meals", "periods", "schedules"]
    database = get_database()
    for key in keys:
        assert key in database.keys()
