/**
 * Adjust the indices of form fields when removing items.
 */
function adjustIndices(componentType) {
    let forms = $(".subform").filter(function() {return this.id.indexOf(componentType) >= 0});
    let currentIndex = 0;

    forms.each(function() {
        const form = $(this);
        const oldIndex = parseInt(form.attr("data-index"));

        form.attr("data-index", currentIndex);
        form.attr("id", form.attr("id").replace(oldIndex, currentIndex));
        $('select, input', form).each(function () {
            this.id = this.id.replace(oldIndex, currentIndex);
            this.name = this.name.replace(oldIndex, currentIndex);
        });
        $('label', form).each(function () {
            $(this).attr("for", $(this).attr("for").replace(oldIndex, currentIndex));
        });
        $('button', form).each(function () {
            $(this).attr("onclick", $(this).attr("onclick").replace(oldIndex, currentIndex));
        });

        currentIndex++;
    });
}

/**
 * Remove a subform.
 */
function removeComponent(id, componentType) {
    let removedForm = $(`#${id}`);
    let index = parseInt(removedForm.attr("data-index"));
    if (index !== 0) {
        removedForm.remove();
        adjustIndices(componentType);
    }
}

/**
 * Add a new subform.
 */
function addComponent(componentType) {
    let lastComponent = $(".subform").filter(function() {return this.id.indexOf(componentType) >= 0}).last();
    let lastIndex = parseInt(lastComponent.attr("data-index"));
    let newComponent = lastComponent.clone();
    // Add the remove button if this is the first new item (e.g. the second item in the list)
    if (lastIndex === 0)
        newComponent.append(`<button type="button" onClick="removeComponent('${componentType}-${lastIndex}-form', '${componentType}');">X</button>`);
    newComponent.appendTo($(`#${componentType}`));
    adjustIndices(componentType);
    // Reset values for the form since you cloned it
    newComponent = $(".subform").filter(function() {return this.id.indexOf(componentType) >= 0}).last();
    let newIndex = parseInt(newComponent.attr("data-index"));
    $(`#${componentType}-${newIndex}-quantity`).attr("value", "1.000").prop("value", "1.000")
    $(`#${componentType}-${newIndex}-unit`).attr("value", "each").prop("value", "each")
    $(`#${componentType}-${newIndex}-state`).attr("value", "raw").prop("value", "raw")
    $(`#${componentType}-${newIndex}-variety`).attr("value", "").prop("value", "")
}

$(document).ready(function() {
    $("[id^=seasoning]").change(function() {
        $("#no_seasoning").attr("value", "n").prop("value", "n").prop("checked", false);
    });
    $("[id^=components]").change(function() {
        $("#no_components").attr("value", "n").prop("value", "n").prop("checked", false);
    });
});
