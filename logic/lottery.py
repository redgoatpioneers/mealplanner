# -*- coding: utf-8 -*-
"""
This module is used to randomly pull food objects from a database.
"""

import random
from data import persistence as db


def filter_objects(objects: list, attribute: str, values: list):
    """
    Filter a list of objects by attribute (e.g. dishes with specific labels).
    Args:
        objects (list): The list of objects that will be filtered.
        attribute (string): The attribute to filter the objects by.
        values (list): The list of values for the attribute to filter the objects by.
    Returns:
        list: A filtered list of objects having the attributes provided.
    """
    filtered_objects = []
    for value in values:
        for obj in objects:
            if isinstance(obj[attribute], list):
                if value.lower() in [value.lower() for value in obj[attribute]]:
                    filtered_objects.append(obj)
            else:
                if value.lower() == obj[attribute].lower():
                    filtered_objects.append(obj)
    return filtered_objects


def get_random_dish(labels=None, sizes=None, categories=None, efforts=None, costs=None, max_time=None):
    """
    Retrieve a random dish.

    Args:
        labels (list): (Optional) Limit the dish returned by label.
        sizes (list): (Optional) Limit the dish returned by size.
        categories (list): (Optional) Limit the dish returned by category.
        efforts (list): (Optional) Limit the dish returned by effort.
        costs (list): (Optional) Limit the dish returned by cost.
        max_time (int): (Optional) Limit the dish returned by maximum number of minutes.
    Returns:
        list: a JSON object representation of a single random dish
    """
    candidates = db.get_dishes()
    if labels:
        candidates = filter_objects(candidates, "labels", labels)
    if sizes:
        candidates = filter_objects(candidates, "size", sizes)
    if categories:
        candidates = filter_objects(candidates, "category", categories)
    if efforts:
        candidates = filter_objects(candidates, "effort", efforts)
    if costs:
        candidates = filter_objects(candidates, "cost", costs)
    if max_time:
        candidates = filter(lambda c: c.time < max_time, candidates)
    if not candidates or len(candidates) == 0:
        return None
    dish = random.choice(candidates)
    return dish


def get_random_meal():
    """
    Retrieve a random meal.

    Returns:
        list: a JSON object representation of a single random meal
    """
    all_meals = db.get_meals()
    return random.choice(list(all_meals))


def get_random_meals(count=1):
    """
    Retrieve a specific number of random meals.

    Returns:
        list: a JSON list of random meals
    """
    random_meals = []
    for i in range(count):
        random_meals.append(get_random_meal())

    return random_meals
