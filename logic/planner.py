# -*- coding: utf-8 -*-
"""
This module is used to build a meal plan from a schedule of periods.
"""

from data import persistence as db
from logic import lottery


def build_mealplan_from_schedule(schedule_name: str = None, plan_name: str = None):
    """
    Build a meal plan from a given schedule.
    If no schedule is provided, use the first one in the database.

    Args:
        schedule_name (string): (Optional) The name of the schedule for the meal plan.
        plan_name (string): (Optional) A name for the meal plan.
    Returns:
        dictionary: A meal plan.
    """
    if schedule_name is None:
        schedule = db.get_schedules()[0]
    else:
        schedule = db.get_schedules([schedule_name])[0]

    if schedule is None:
        return None

    schedule_mealplan = {"name": plan_name if plan_name else "Meal Plan"}

    for period_label, period_name in schedule["schedule"].items():  # Create a plan for each period
        period_mealplan = {}
        period = db.get_periods([period_name])[0]
        for food_label, food_def in period["sequence"].items():  # Randomly assign a food item for each food definition
            if food_def["type"] == "Dish":
                food = lottery.get_random_dish(
                    labels=food_def["labels"] if "labels" in food_def else None,
                    sizes=[food_def["size"]] if "size" in food_def else None,
                    categories=[food_def["category"]] if "category" in food_def else None,
                    efforts=[food_def["effort"]] if "effort" in food_def else None,
                    costs=[food_def["cost"]] if "cost" in food_def else None,
                    max_time=food_def["time"] if "time" in food_def else None)
            elif food_def["type"] == "Meal":
                food = lottery.get_random_meal()
            else:
                food = None  # TODO: Handle this case better... throw an exception?
            period_mealplan[food_def["name"]] = food
        schedule_mealplan[period_label] = period_mealplan

    return schedule_mealplan
